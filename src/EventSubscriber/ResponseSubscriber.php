<?php

namespace Drupal\ajax_debug\EventSubscriber;

use Drupal\Core\Ajax\AjaxResponse;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Class KernelEventSubscriber.
 *
 * @package Drupal\symfony_debug\EventSubscriber
 */
class ResponseSubscriber implements EventSubscriberInterface {

  /**
   * KernelEvents::RESPONSE event handler.
   *
   * @param \Symfony\Component\HttpKernel\Event\ResponseEvent $event
   *   Event.
   *
   * @see \Drupal\Core\EventSubscriber\AjaxResponseSubscriber::onResponse()
   */
  public function onResponse(ResponseEvent $event) {
    $response = $event->getResponse();
    if ($response instanceof AjaxResponse) {
      // We undo what AjaxResponseSubscriber does.
      $response->headers->set('Content-Type', 'application/json; charset=utf-8');
      $response->setContent(
        rtrim(
          ltrim(
            $response->getContent(),
            '<textarea>'
          ),
          '</textarea>'
        )
      );
    }
  }

  /**
   * Get subscribed events.
   *
   * @return array
   *   Subscribed events.
   */
  public static function getSubscribedEvents(): array {
    return [
      // Just after AjaxResponseSubscriber.
      KernelEvents::RESPONSE => ['onResponse', -101],
    ];
  }

}
