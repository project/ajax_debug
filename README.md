AJAX debug
==========

INTRODUCTION
------------

This module removes the `textarea` tag that Drupal adds to AJAX JSON response.
This makes debugging the content of these responses easier.

 * For a full description of the module, visit the project page:
   <https://www.drupal.org/project/ajax_debug>

 * To submit bug reports and feature suggestions, or track changes:
   <https://www.drupal.org/project/issues/ajax_debug>


REQUIREMENTS
------------

No special requirements.


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit
   <https://www.drupal.org/documentation/install/modules-themes/modules-8>
   for further information.


CONFIGURATION
-------------

The module has no menu or modifiable settings. There is no configuration. When
enabled, the module will automatically rewrite JSON responses.


TROUBLESHOOTING
---------------

If AJAX responses are corrupted,
it means your browser actually needs the `textarea` hack
so you should not use this module.


MAINTAINERS
-----------

Current maintainers:

* Pierre Rudloff (prudloff) - <https://www.drupal.org/u/prudloff>

This project has been sponsored by:

 * Insite:
    A lasting and independent business project
    with more than 20 years experience
    and several hundred references in web projects and visual communication
    with public actors, associative and professional networks.
    Visit <https://www.insite.coop/> for more information.
